import "./App.css";
import React from "react";

import Table from "./components/Table";

function App() {
  return (
    <div className="App">
      <h1> Star Wars Planets </h1>
      <Table />
    </div>
  );
}

export default App;
