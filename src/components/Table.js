import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import FilterMenu from "./FilterMenu";
import Loader from "./Loader";

const useStyles = makeStyles({
  table: {
    maxWidth: 380,
  },
  tableContainer: {
    display: "flex",
    justifyContent: "center",
  },
  tableBody: {
    color: "orange",
    textColor: "red",
  },
  tableCell: {
    display: "flex",
    alignItems: "center",
  },
});

export default function BasicTable() {
  const [data, setData] = useState([]);
  const [dataToShow, setDataToShow] = useState([data]);
  const [sort, setSort] = useState(false);
  const [loading, setLoading] = useState(true);
  const [direction, setDirection] = useState("desc");

  const classes = useStyles();

  // Formattierung für Zahlen
  const formatter = new Intl.NumberFormat("de", {
    notation: "compact",
  });

  // Fetch Data (6 Endpunkte um alles auf einer Seite darzustellen)
  const getData = async () => {
    const urls = [
      "https://swapi.dev/api/planets/",
      "https://swapi.dev/api/planets/?page=2",
      "https://swapi.dev/api/planets/?page=3",
      "https://swapi.dev/api/planets/?page=4",
      "https://swapi.dev/api/planets/?page=5",
      "https://swapi.dev/api/planets/?page=6",
    ];
    try {
      setLoading(true);
      let res = await Promise.all(urls.map((e) => fetch(e)));
      let json = await Promise.all(res.map((e) => e.json()));
      let arrayToBeMerged = json.map((e) => e.results);
      let mergedArray = [].concat.apply([], arrayToBeMerged);
      setData(mergedArray);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    setDataToShow(data);
  }, [data]);

  // Filter und Sortierungen
  const sortAlphabetic = () => {
    if (sort === false) {
      setDataToShow(dataToShow.sort((a, b) => a.name.localeCompare(b.name)));
      setSort(true);
      setDirection("asc");
    } else {
      setDataToShow(dataToShow.sort((a, b) => b.name.localeCompare(a.name)));
      setSort(false);
      setDirection("desc");
    }
  };

  const filterPopulationLess = () => {
    setDataToShow(data.filter((planet) => planet.population < 10000000));
  };
  const filterPopulationGreater = () => {
    setDataToShow(data.filter((planet) => planet.population > 10000000));
  };

  const allPlanets = () => {
    setDataToShow(data);
  };

  return (
    <TableContainer className={classes.tableContainer}>
      {loading ? (
        <Loader />
      ) : (
        <TableContainer className={classes.table} aria-label="simple table">
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  <TableSortLabel
                    onClick={sortAlphabetic}
                    active={true}
                    direction={direction}
                  >
                    <strong>Planet</strong>
                  </TableSortLabel>
                </TableCell>

                <TableCell align="right" className={classes.tableCell}>
                  <strong>Population</strong>
                  <FilterMenu
                    filterPopulationLess={filterPopulationLess}
                    filterPopulationGreater={filterPopulationGreater}
                    allPlanets={allPlanets}
                  />
                </TableCell>
                <TableCell align="right">
                  <strong>Diameter</strong>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody className={classes.tableBody}>
              {dataToShow.map((planet, i) => (
                <TableRow key={i}>
                  <TableCell component="th" scope="row">
                    {planet.name}
                  </TableCell>
                  <TableCell align="right">
                    {planet.population === "unknown"
                      ? "unknown"
                      : formatter.format(planet.population)}
                  </TableCell>
                  <TableCell align="right">
                    {planet.diameter === "unknown"
                      ? "unknown"
                      : formatter.format(planet.diameter)}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </TableContainer>
  );
}
